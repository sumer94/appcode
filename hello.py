from flask import Flask, render_template, request
import sqlite3 as sql
app = Flask(__name__)

import sqlite3
conn = sqlite3.connect('database.sqlite')
#print "Opened database successfully";

#conn.execute('CREATE TABLE employee (id INT PRIMARY KEY NOT NULL,name TEXT NOT NULL, city TEXT NOT NULL)')

#conn.close()


@app.route('/')
def home():
   return render_template('person.html')

@app.route('/enternew')
def new_person():
   return render_template('person.html')

@app.route('/addrec',methods = ['POST', 'GET'])
def addrec():
   if request.method == 'POST':
      try:
         nm = request.form['nm']
         
         city = request.form['city']

         
         
         with sql.connect("database.sqlite") as con:
            cur = con.cursor()
            
            cur.execute("INSERT INTO Persons (name,city) VALUES(?,?)",(nm,city))
                        
            con.commit()
            msg = "Record added successfully!!"
      except:
         con.rollback()
         msg = "error in insert operation"
      
      finally:
         return render_template("result.html",msg = msg)
         con.close()

@app.route('/list')
def list():
   con = sql.connect("database.sqlite")
   con.row_factory = sql.Row
   
   cur = con.cursor()
   cur.execute("select * from Persons")
   
   rows = cur.fetchall();
   return render_template("list.html",rows = rows)

@app.route('/delete')   
def delete():
	conn.execute("DELETE from Persons where name = 'hasnain';")
	conn.commit()
	return "Deleted"

@app.route('/update')
def update():
   conn = sqlite3.connect("database.sqlite")
   conn.execute("UPDATE Persons set name='ahmed umer' where name ='umer'; ")
   conn.commit()
   return "updated"
	
if __name__ == '__main__':
   app.run(debug = True)






# from flask import Flask, request, flash, url_for, redirect, render_template
# from flask_sqlalchemy import SQLAlchemy
# import os
# import sqlite3

# from hello import __init__
# import sqlite3
# conn = sqlite3.connect('database.sqlite')
#print "Opened database successfully";

# conn.execute('CREATE TABLE Persons (name TEXT, city TEXT)')

# conn.close()



# app = Flask(__name__)
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///Persons.sqlite'
# app.config['SECRET_KEY'] = "random string"

# db = SQLAlchemy(app)

# class Persons(db.Model):
#    id = db.Column('person_id', db.Integer, primary_key = True)
#    name = db.Column(db.String(100))
#    city = db.Column(db.String(50))
  
   

# def __init__(self, name, city):
#    self.name = name
#    self.city = city

# @app.route('/enternew')
# def new_person():
#    return render_template('person.html')   
   
# @app.route('/')
# def show_all():
#    return render_template('show_all.html', Persons = Persons.query.all() )


# @app.route('/new', methods = ['GET', 'POST'])
# def new():
#    if request.method == 'POST':
#       if not request.form['name'] or not request.form['city']:
#          flash('Please enter all the fields', 'error')
#       else:
#          person = Persons(request.form['name'], request.form['city'])
         
#          db.session.add(person)
#          db.session.commit()
#          flash('Record was successfully added')
#          return redirect(url_for('show_all'))
#          return render_template('new.html')

# if __name__ == '__main__':
#    db.create_all()
#    app.run(debug = True)

   

# # @app.route('/')
# # def index():
# # 	return "method used: %s" % request.method


# # @app.route('/hello')

# # def foo():
# # 	return '<h2>Hello World</h2>'

# # @app.route('/profile/<username>')

# # def profile(username):
# # 	return "<h2> Hello %s</h2>" % username

# # @app.route('/pro/<name>')

# # def pro(name):
# # 	return render_template("name.html",name=name)

# # @app.route('/profile/<int:id>')

# # def post(id):
# # 	return "<h2> your id is: %s </h2>" % id	

# # if __name__ == "__main__":
# # 	app.run(debug=true)
# 	